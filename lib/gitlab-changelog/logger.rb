require_relative 'json_parser'
require_relative 'enviroment'
# Creates the log text, saves it into a file
module GitlabChangelog
  # Logger class
  class Logger
    class << self
      # generates and saves the changelog into a file
      def generate_log
        set_enviroment
        create_log_text
        save_log_into_file
      end

      private

      def set_enviroment
        enviroment = Enviroment.new
        @merge_requests = JSONParser.merge_requests(enviroment)
      end

      def create_log_text
        @log_text = "# Changelog\n\n"
        @merge_requests.each do |merge_request|
          map_labels = merge_request.labels.map { |label| "[#{label}]" }
          labels = map_labels.join(' ')
          @log_text << "## #{labels} #{merge_request.title}\n"
          @log_text << "- #{merge_request.description} \n" unless merge_request.description.empty?
          @log_text << "\n"
        end
      end

      def save_log_into_file
        log = File.join(Pathname.new('.'), 'CHANGELOG.md')
        File.open(log, 'w+') { |file| file.write(@log_text) }
      end
    end
  end
end
