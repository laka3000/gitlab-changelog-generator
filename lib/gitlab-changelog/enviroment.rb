require 'yaml'
require 'pathname'
require 'erb'
# Sets up the enviroment, including variables and URLs
module GitlabChangelog
  # Enviroment class
  # @merge_request_uri: the URI that will request the merge requests.
  class Enviroment
    attr_reader :merge_request_uri

    def initialize
      @variables = YAML.safe_load(ERB.new(IO.read(config_file_path)).result)
      @merge_request_uri = set_url
    end

    protected

    def set_url
      URI("#{base_url}/#{@variables['project_id']}/#{merge_arg}&#{token_arg}")
    end

    def config_file_path
      Pathname.new('.').join(ARGV.first)
    end

    private
    
    def base_url
      "#{@variables['gitlab_url']}api/v4/projects"
    end

    def token_arg
      "private_token=#{@variables['private_token']}"
    end

    def merge_arg
      'merge_requests?state=merged'
    end

  end
end
