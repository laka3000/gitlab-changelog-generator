require_relative 'merge_request'
require 'json'
require 'net/http'
# Takes care of JSON requests
module GitlabChangelog
  # JSONParser class
  class JSONParser
    class << self
      # returns the merge requests as MergeRequest
      def merge_requests(enviroment)
        merge_requests = request_json(enviroment.merge_request_uri).map do |json_merge_request|
          merge_request_from_json(json_merge_request)
        end
      end

      private

      def merge_request_from_json(jmr)
        MergeRequest.new(jmr['title'], jmr['description'], jmr['labels'])
      end

      def request_json(uri)
        response = Net::HTTP.get_response(uri)
        data = JSON.parse(response.body)
        raise_error(data) unless response.code.eql? '200'
        data
      end

      def raise_error(data)
        raise Net::HTTPBadResponse, data['message']
      end
    end
  end
end
