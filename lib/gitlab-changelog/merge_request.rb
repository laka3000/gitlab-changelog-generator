# Takes care of the merge requests storage and parsing
module GitlabChangelog
  # Merge request class
  # @title - Merge request title
  # @description - Merge request description
  # @labels - Merge request labels
  class MergeRequest
    attr_accessor :title, :description, :labels

    def initialize(title, description, labels)
      @title = title
      @description = description
      @labels = labels
    end
  end
end
