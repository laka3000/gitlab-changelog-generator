require 'gitlab-changelog/logger'

# Generates a MD changelog file from a gitlab project
module GitlabChangelog
  def self.generate_log
    Logger.generate_log
  end
end
