
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name          = 'gitlab-changelog'
  spec.version       = '1.2.1'
  spec.authors       = ['Alexandre Karpinski Manikowski']
  spec.email         = ['alexandre.karpinski.m@gmail.com']
  spec.summary       = 'Generates a MD changelog file from a gitlab project'
  spec.homepage      = 'https://gitlab.com/laka3000/gitlab-changelog-generator/'
  spec.license       = 'MIT'
  spec.bindir        = 'exe'
  spec.executables << 'gitlab-changelog'
  spec.require_paths = ['lib']
  spec.add_development_dependency 'bundler', '~> 1.16'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
end
