Gitlog
==========

Gitlog is a gem that will generate a change file in markdown format, collecting all the merge resquest and tags from a Gitlab project.

## Setup

To install the Gitlog Gem, edit your `Gemfile`, add the following line

    gem 'gitlab-changelog', :git => "https://gitlab.com/laka3000/gitlab-changelog-generator.git"

Then run

`bundle install`

## Configuring the gem

Create a YAML file inside of your application folder.

The YAML file should look like this:

```
gitlab_url: 'https://gitlab.com/'
private_token: ''
project_id: ''
```

Set the variables accordingly, as follows:

* gitlab_url:
	Default is "https://gitlab.com/". Change if you are inside of a group/organization with a custom url

* private_token:
	Your gitlab project access token, you can learn how to generate one at https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html
	Make sure the token scope is set to "API"

* project_id:
	The numeric ID from the project you want to generate the changelog for.
	You can find the project ID number at "General project settings", by going into the Settings tab of the project

## Running the gem

After configuring this file, run this in the terminal, passing the location of your YAML file as an argument

`gitlab-changelog "your_YAML_file.yml"`

This will create a `CHANGELOG.md` file in the root directory of you application
